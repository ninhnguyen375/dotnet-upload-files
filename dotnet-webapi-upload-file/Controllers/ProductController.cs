using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_webapi_upload_file.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        [HttpPost]
        public async Task<ActionResult> createNewProduct([FromForm] SaveProductDTO product)
        {
            List<IFormFile> images = product.Images;
            List<string> imageUrls = new List<string>();

            foreach (var image in images)
            {
                string rootImagePath = "wwwroot/product_images/";
                string publicPath = "/product_images/" + product.Id + image.FileName;
                string filePath = rootImagePath + product.Id + image.FileName;

                // Create directory if not Exists
                if (!Directory.Exists(rootImagePath))
                {
                    Directory.CreateDirectory(rootImagePath);
                }

                using (var stream = System.IO.File.Create(filePath))
                {
                    await image.CopyToAsync(stream);
                }
                imageUrls.Add(publicPath);
            }
            Product createdProduct = new Product
            {
                Id = product.Id,
                Name = product.Name,
                ImageUrls = imageUrls
            };

            return Ok(new { createdProduct });
        }
    }

    public class SaveProductDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<IFormFile> Images { get; set; }
    }
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> ImageUrls { get; set; }
    }
}